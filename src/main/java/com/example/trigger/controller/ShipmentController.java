package com.example.trigger.controller;

import com.example.trigger.model.dto.ShipmentDto;
import com.example.trigger.service.ShipmentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shipment")
public class ShipmentController {

    private final ShipmentService shipmentService;

    public ShipmentController(ShipmentService shipmentService) {
        this.shipmentService = shipmentService;
    }

    @PostMapping
    public ShipmentDto create(@RequestBody ShipmentDto shipmentDto) {
        return shipmentService.create(shipmentDto);
    }

}
