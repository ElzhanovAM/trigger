package com.example.trigger.controller;

import com.example.trigger.model.dto.EntityCommandType;
import com.example.trigger.model.dto.EntityUpdate;
import com.example.trigger.model.dto.VirtualSensorDto;
import com.example.trigger.service.PairingEventService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trigger")
public class TriggerRestController {

    private final PairingEventService pairingEventService;

    public TriggerRestController(PairingEventService pairingEventService) {
        this.pairingEventService = pairingEventService;
    }

    @PostMapping
    public VirtualSensorDto entityUpdate(@RequestBody EntityUpdate entityUpdate) {
        if (entityUpdate.getCommand().getEntityCommandType().equals(EntityCommandType.SHIPMENT_CREATE)) {
            return pairingEventService.createVirtualSensorByShipmentId(entityUpdate.getEntityId());
        }
        return null;
    }
}
