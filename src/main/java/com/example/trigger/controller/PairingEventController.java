package com.example.trigger.controller;

import com.example.trigger.model.dto.PairingEventDto;
import com.example.trigger.service.PairingEventService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pairing_event")
public class PairingEventController {

    private final PairingEventService pairingEventService;

    public PairingEventController(PairingEventService pairingEventService) {
        this.pairingEventService = pairingEventService;
    }

    @PostMapping
    public PairingEventDto create(@RequestBody PairingEventDto pairingEventDto) {
        return pairingEventService.create(pairingEventDto);
    }

}
