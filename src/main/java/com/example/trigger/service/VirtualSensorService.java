package com.example.trigger.service;


import com.example.trigger.model.dto.VirtualSensorDto;

public interface VirtualSensorService {
    VirtualSensorDto create(VirtualSensorDto virtualSensorDto);
}
