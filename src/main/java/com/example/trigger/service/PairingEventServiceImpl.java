package com.example.trigger.service;

import com.example.trigger.dao.PairingEventDao;
import com.example.trigger.model.dto.PairingEventDto;
import com.example.trigger.model.dto.VirtualSensorDto;
import com.example.trigger.model.entity.PairingEventEntity;
import com.example.trigger.model.entity.VirtualSensorEntity;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class PairingEventServiceImpl implements PairingEventService {

    private final ShipmentService shipmentService;
    private final PairingEventDao pairingEventDao;
    private final VirtualSensorService virtualSensorService;

    public PairingEventServiceImpl(ShipmentService shipmentService,
                                   PairingEventDao pairingEventDao,
                                   VirtualSensorService virtualSensorService) {
        this.shipmentService = shipmentService;
        this.pairingEventDao = pairingEventDao;
        this.virtualSensorService = virtualSensorService;
    }

    @Override
    public VirtualSensorDto createVirtualSensorByShipmentId(UUID shipmentId) {
        val shipment = shipmentService.getById(shipmentId);
        if (shipment != null) {
            return pairingEventDao.findPairingEventEntityByPackageId(shipment.getKnPackageId().getPackageId())
                    .map(pairingEventEntity -> {
                        val entity = new VirtualSensorEntity();

                        entity.setShipmentId(shipmentId);
                        entity.setDeviceId(pairingEventEntity.getDeviceId());
                        return virtualSensorService.create(
                                new VirtualSensorDto()
                                        .setDeviceId(pairingEventEntity.getDeviceId())
                                        .setShipmentId(shipmentId)
                        );
                    })
                    .orElse(null);

        }
        return null;
    }

    @Override
    public PairingEventDto create(PairingEventDto pairingEventDto) {
        return pairingEventDao.save(PairingEventEntity.fromDto(pairingEventDto)).toDto();
    }
}
