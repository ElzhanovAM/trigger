package com.example.trigger.service;

import com.example.trigger.dao.VirtualSensorDao;
import com.example.trigger.model.dto.VirtualSensorDto;

import com.example.trigger.model.entity.VirtualSensorEntity;
import org.springframework.stereotype.Service;

@Service
public class VirtualSensorServiceImpl implements VirtualSensorService {

    private final VirtualSensorDao virtualSensorDao;

    public VirtualSensorServiceImpl(VirtualSensorDao virtualSensorDao) {
        this.virtualSensorDao = virtualSensorDao;
    }

    @Override
    public VirtualSensorDto create(VirtualSensorDto virtualSensorDto) {
        return virtualSensorDao.save(VirtualSensorEntity.fromDto(virtualSensorDto)).toDto();
    }
}
