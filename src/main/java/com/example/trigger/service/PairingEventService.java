package com.example.trigger.service;

import com.example.trigger.model.dto.PairingEventDto;
import com.example.trigger.model.dto.VirtualSensorDto;

import java.util.UUID;

public interface PairingEventService {
    VirtualSensorDto createVirtualSensorByShipmentId(UUID shipmentId);

    PairingEventDto create(PairingEventDto pairingEventDto);
}
