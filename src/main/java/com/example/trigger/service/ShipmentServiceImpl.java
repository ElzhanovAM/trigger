package com.example.trigger.service;

import com.example.trigger.dao.ShipmentDao;
import com.example.trigger.model.dto.ShipmentDto;
import com.example.trigger.model.entity.ShipmentEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    private final ShipmentDao shipmentDao;

    public ShipmentServiceImpl(ShipmentDao shipmentDao) {
        this.shipmentDao = shipmentDao;
    }

    public ShipmentDto getById(UUID shipmentId) {
        return shipmentDao.findById(shipmentId)
                .map(ShipmentEntity::toDto)
                .orElse(null);
    }

    @Override
    public ShipmentDto create(ShipmentDto shipmentDto) {
        return shipmentDao.save(ShipmentEntity.fromDto(shipmentDto)).toDto();
    }
}
