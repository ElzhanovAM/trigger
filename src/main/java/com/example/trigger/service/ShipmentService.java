package com.example.trigger.service;

import com.example.trigger.model.dto.ShipmentDto;

import java.util.UUID;

public interface ShipmentService {

    ShipmentDto getById(UUID shipmentId);

    ShipmentDto create(ShipmentDto shipmentDto);
}
