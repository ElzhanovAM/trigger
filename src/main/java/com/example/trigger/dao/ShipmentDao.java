package com.example.trigger.dao;


import com.example.trigger.model.entity.ShipmentEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ShipmentDao extends CrudRepository<ShipmentEntity, UUID> {
}
