package com.example.trigger.dao;


import com.example.trigger.model.entity.VirtualSensorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface VirtualSensorDao extends JpaRepository<VirtualSensorEntity, UUID> {
}
