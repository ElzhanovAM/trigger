package com.example.trigger.dao;

import com.example.trigger.model.entity.PairingEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PairingEventDao extends JpaRepository<PairingEventEntity, String> {
    Optional<PairingEventEntity> findPairingEventEntityByPackageId(@Param("package_id") String packageId);
}
