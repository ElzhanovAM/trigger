package com.example.trigger.model.entity;

import com.example.trigger.model.dto.KnPackageId;
import com.example.trigger.model.dto.ShipmentDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Table(name = "shipment")
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(name = "package_id")
    private String packageId;
    @Column(name = "kn_com_ref")
    private String knComRef;

    public ShipmentDto toDto() {
        return new ShipmentDto()
                .setId(id)
                .setKnPackageId(
                        new KnPackageId()
                                .setPackageId(packageId)
                                .setKnComRef(knComRef)
                );
    }

    public static ShipmentEntity fromDto(ShipmentDto shipmentDto) {
        val entity = new ShipmentEntity();
        entity.setPackageId(shipmentDto.getKnPackageId().getPackageId());
        entity.setKnComRef(shipmentDto.getKnPackageId().getKnComRef());
        return entity;
    }
}
