package com.example.trigger.model.entity;

import com.example.trigger.model.dto.PairingEventDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "pairing_event")
@NoArgsConstructor
@AllArgsConstructor
public class PairingEventEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(name = "package_id")
    private String packageId;
    @Column(name = "device_id")
    private UUID deviceId;
    @Column
    private OffsetDateTime date;

    public static PairingEventEntity fromDto(PairingEventDto pairingEventDto) {
        val entity = new PairingEventEntity();
        entity.setDeviceId(pairingEventDto.getDeviceId());
        entity.setPackageId(pairingEventDto.getPackageId());
        entity.setDate(pairingEventDto.getDate());
        return entity;
    }

    public PairingEventDto toDto() {
        return new PairingEventDto()
                .setId(id)
                .setPackageId(packageId)
                .setDeviceId(deviceId)
                .setDate(date);
    }
}
