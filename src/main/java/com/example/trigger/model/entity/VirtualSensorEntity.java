package com.example.trigger.model.entity;

import com.example.trigger.model.dto.VirtualSensorDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Table(name = "virtual_sensor")
@NoArgsConstructor
@AllArgsConstructor
public class VirtualSensorEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(name = "shipment_id")
    private UUID shipmentId;

    @Column(name = "device_id")
    private UUID deviceId;

    public VirtualSensorDto toDto() {
        return new VirtualSensorDto().setId(id)
                .setShipmentId(shipmentId)
                .setDeviceId(deviceId);
    }

    public static VirtualSensorEntity fromDto(VirtualSensorDto virtualSensorDto) {
        val entity = new VirtualSensorEntity();
        entity.setDeviceId(virtualSensorDto.getDeviceId());
        entity.setShipmentId(virtualSensorDto.getShipmentId());
        return entity;
    }
}
