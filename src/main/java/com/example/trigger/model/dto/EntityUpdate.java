package com.example.trigger.model.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class EntityUpdate {
    private Date timestamp;
    private UUID entityId;
    private EntityCommand command;
    private String userName;
}
