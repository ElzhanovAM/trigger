package com.example.trigger.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class KnPackageId {
    private String knComRef;
    private String packageId;
}
