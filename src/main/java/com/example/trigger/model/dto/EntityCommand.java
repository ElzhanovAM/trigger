package com.example.trigger.model.dto;

import lombok.Data;

@Data
public class EntityCommand {
    private EntityCommandType entityCommandType;
    private DataSource dataSource;
}
