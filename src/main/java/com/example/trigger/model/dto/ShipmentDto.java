package com.example.trigger.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class ShipmentDto {

    private UUID id;
    private KnPackageId knPackageId;

}
