package com.example.trigger.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class VirtualSensorDto {
    private UUID id;
    private UUID shipmentId;
    private UUID deviceId;
}
