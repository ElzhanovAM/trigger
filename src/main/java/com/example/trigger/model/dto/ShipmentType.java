package com.example.trigger.model.dto;

public enum ShipmentType {
    SHIPMENT,
    TRANSPORT
}
