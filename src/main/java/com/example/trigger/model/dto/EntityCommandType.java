package com.example.trigger.model.dto;

public enum EntityCommandType {
    SHIPMENT_CREATE(ShipmentType.SHIPMENT),
    TRANSPORT_CREATE(ShipmentType.TRANSPORT);

    private ShipmentType shipmentType;

    EntityCommandType(ShipmentType shipmentType) {
        this.shipmentType = shipmentType;
    }
}
