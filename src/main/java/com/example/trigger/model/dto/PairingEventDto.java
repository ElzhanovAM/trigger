package com.example.trigger.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class PairingEventDto {
    private UUID id;
    private String packageId;
    private UUID deviceId;
    private OffsetDateTime date;
}
